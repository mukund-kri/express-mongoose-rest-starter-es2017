'use strict';


export const replacer = function(key, value) {
  if(key == '__v') {
    return undefined;
  } else {
    return value;
  }
};

export default {
  replacer
}
