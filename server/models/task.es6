'use strict';


import mongoose from 'mongoose';

var taskSchema = mongoose.Schema({
  text:       { type:String, required: true},
  createTime: { type: Date, default: Date.now },
  status:     { type: Boolean, default: false }
});


export default mongoose.model('Task', taskSchema);
