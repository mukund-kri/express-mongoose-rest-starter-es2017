'use strict';


import express from 'express';
import createError from 'http-errors';

import Task from '../models/task';


const router = express.Router();
const wrap = fn => (req, res, next) => fn(req, res, next).catch(next);


router.get('/', wrap(async function(req, res, next) {
  let task = await Task.find({});
  res.json(task);
}));

router.get('/:id', wrap(async function(req, res, next) {
  let docId = req.params.id;
  let task = await Task.findById(docId);

  if(task) res.json(task);
  else throw createError(404, 'Task not found');
}));

router.post('/', wrap(async function(req, res, next) {
  let task = new Task({
    text: req.body.text
  });
  await task.save();
  res.json({ success: 'Task created', task: task });
}));

router.delete('/:id', wrap(async function(req, res, next) {
  let docId = req.params.id;
  let task = await Task.findByIdAndRemove(docId);

  if(task) res.json({ success: 'Task deleted'});
  else throw createError(404, 'Task not found');
}));

router.put('/:id', wrap(async function(req, res, next) {
  let docId = req.params.id;

  let updateQuery = { $set: {} };
  for(let prop in req.body) {
    updateQuery.$set[prop] = req.body[prop];
  }
  let task = await Task.findByIdAndUpdate(docId, updateQuery);

  if(task) res.json({ success: 'Task updated'});
  else throw createError(404, 'Task not found');
}));




// Error handling for this api
router.use((err, req, res, next) => {
  if(err instanceof createError.NotFound) {
    // Task not found
    res.status(err.statusCode).json({ error: err.message });
  } else {
    // We don't want the end user to see our exception.
    console.log(err);
    res.status(500).json({ error: 'Internal error' });
  }
});


export default router;
