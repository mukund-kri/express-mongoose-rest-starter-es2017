'user strict';

import path from 'path';
import mongoose from 'mongoose';
import Promise from 'bluebird';

mongoose.Promise = Promise;


export const mongoConfig = {
  dev: {
    url: 'mongodb://localhost/devdb'
  },
  test: {
    url: 'mongodb://localhost/testdb'
  }
};

export const expressConfig = {
  'views':       path.resolve(__dirname, '../', 'views'),
  'view engine': 'pug'
};

export default {
  mongoConfig,
  expressConfig
}
