# Express+Mongoose REST api in cutting edge JS #

A starter project for writing REST api with ExpressJS and MongooseJS, witten
with the latest JS, which includes es2017, stage-0 code. Used babel for
transpilation.


## Features ##

 1. Vagrant

 1. node 6

 I use Node 6.x in this vagrant. Though any node supporting babel can be used.

 1. List of Frameworks/Libs
  * ExpressJS
  * MongooseJS
  * mocha
  * chai
  * gulp
