'use strict';

import { expect } from 'chai';
import  supertest  from 'supertest-as-promised';
// const supertest = require('supertest');

import { app } from '../../../server/app';


describe('#Home routes', () => {
  it('#Home page should render correctly', async function() {
    let response = await supertest(app)
      .get('/')
      .expect('content-type', /text/)
      .expect(200)

    expect(response.text).to.match(/Your\ App/)
  });
});
