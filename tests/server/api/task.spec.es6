'use strict';

import { expect } from 'chai';
import mongoose from 'mongoose';
import supertest from 'supertest-as-promised';
import fixtures from 'node-mongoose-fixtures';
import mochaMongoose from 'mocha-mongoose';

import Task from '../../../server/models/task';
import { app } from '../../../server/app';
import { mongoConfig } from '../../../server/config/server.config';

import taskFixtures from '../fixtures/many.tasks';


const dbUrl = mongoConfig.test.url;
const clearDb = mochaMongoose(dbUrl, {noClear: true});


describe('Task API', () => {

  before(async function() {
    // if connection to mongo exists, then do nothing
    if(mongoose.connection.db) return;

    // else connect to db
    await mongoose.connect(dbUrl);
  });

  before((done) => {
    fixtures(taskFixtures, done);  // not thenable, no async/await
  });

  after((done) => {
    clearDb(done);    // this api is not thenable
  });

  it('#GET should return app tasks', async function() {
    let res = await supertest(app)
      .get('/api/task')
      .expect(200)
      .expect('content-type', /json/);

    expect(res).deep.property('body.length', 2);
    expect(res).deep.property('body[0].text', 'Task 1');
  });

  it('#POST should create a new record', async function() {
    let res = await supertest(app)
      .post('/api/task')
      .send({ text: 'posted task'})
      .expect(200)
      .expect('content-type', /json/);

    expect(res).deep.property('body.success', 'Task created');

    // Check the database if the task is created
    let task = await Task.find({ text: 'posted task'});
    expect(task[0]).to.be.ok;
    expect(task[0].status).to.equal(false);
  });

  it('#DELETE should remove a record', async function() {
    await supertest(app)
      .delete('/api/task/500000000000000000000001')
      .expect(200)
      .expect('content-type', /json/)
      .expect({success: 'Task deleted'});

    // check the db.
    let task = await Task.findById('500000000000000000000001');
    expect(task).to.not.be.ok;
  });

  it('#DELETE on a non-existant record should be 404', async function() {
    await supertest(app)
    .delete('/api/task/500000000000000000000009')
    .expect(404)
    .expect('content-type', /json/)
    .expect({ error: 'Task not found' });
    return;
  });

  it('#GET with id should return a single task', async function() {
    let res = await supertest(app)
    .get('/api/task/500000000000000000000002')
    .expect(200)
    .expect('content-type', /json/);

    expect(res.body).property('text', 'Task 2');
  });

  it('#PUT should update a document', async function() {
    await supertest(app)
    .put('/api/task/500000000000000000000002')
    .send({ text: 'update text' })
    .expect(200)
    .expect('content-type', /json/)
    .expect({success: 'Task updated'});

    // Check the database
    let task = await Task.findById('500000000000000000000002');
    expect(task).property('text', 'update text');
  });

  it('#PUT on non-existant resource should be a 404', async function() {
    await supertest(app)
    .put('/api/task/500000000000000000000009')
    .send({ text: 'update text 2'})
    .expect(404)
    .expect('content-type', /json/)
    .expect({ error: 'Task not found'});
  });
});
