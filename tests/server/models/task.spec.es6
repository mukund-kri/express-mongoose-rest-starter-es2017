'use strict';

import { expect } from 'chai';
import mongoose from 'mongoose';
import mochaMongoose from 'mocha-mongoose';

import Task from '../../../server/models/task';
import { mongoConfig } from '../../../server/config/server.config';
import { genTaskData } from './faker.helper';

const dbUrl = mongoConfig.test.url;
const clearDb = mochaMongoose(dbUrl, {noClear: true});


describe('Task Model', () => {

  before(async function() {
    if(mongoose.connection.db) return;
    await mongoose.connect(dbUrl);
  });

  after(function(done) {
    // having some problems with async/await on this fuction. TODO: Not promise?
    clearDb(done);
  });


  it('#Instantiate and save', async function() {
    let task = new Task({ text: 'Task 1'});

    await task.save();
    expect(task.__v).to.not.be.undefined;
  });


  it('#Can be created', async function() {
    let fakeTaskData = genTaskData();
    let task = await Task.create(fakeTaskData);

    expect(task.__v).to.not.be.undefined;
    expect(task).property('text', fakeTaskData.text);
  });


  it('#Can be listed', async function() {
    let tasks = await Task.find({});
    expect(tasks.length).to.equal(2);
  });


  // TODO: You know the drill, add more tests here ...
});
