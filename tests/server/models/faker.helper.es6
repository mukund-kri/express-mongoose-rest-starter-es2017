'use strict';

import faker from 'faker';


export var genTaskData = function() {
  return {
    text: faker.lorem.sentence(6)
  }
};

export default {
  genTaskData
}
