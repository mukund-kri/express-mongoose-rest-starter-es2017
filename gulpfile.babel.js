'use strict';


import gulp from 'gulp';
import nodemon from 'gulp-nodemon';
import babel from 'gulp-babel';
import Cache from 'gulp-file-cache';
import mocha from 'gulp-spawn-mocha';
import runSeq from 'run-sequence';
import jshint from 'gulp-jshint';


var cache = new Cache();
const mochaOpts = {
  compilers: [
    'js:babel-core/register'
  ]
};

const SERVER_CODE = './server/**/*.es';
const TEST_DIR = './tests/server';
const TEST_CODE = TEST_DIR + '/**/*.es';

const JSHINT_CONFIG = {
  esversion: 6,
  node: true
};

const JSHINT_TEST_CONFIG = {
  esversion: 6,
  node: true,
  mocha: true
};


/* ----------------------------- CHECKING / STYLE --------------------------- */
gulp.task('lint:servercode', () => {
  return gulp.src(SERVER_CODE)
    .pipe(jshint(JSHINT_CONFIG))
    .pipe(jshint.reporter('default'));
});

gulp.task('lint:tests', () => {
  return gulp.src(TEST_CODE)
    .pipe(jshint(JSHINT_TEST_CONFIG))
    .pipe(jshint.reporter('default'));
});

gulp.task('lint', ['lint:servercode', 'lint:tests']);

/* ------------------------- DEVELOPMENT TASKS ------------------------------ */
// compile es2017 to es5
gulp.task('compile', () => {
  const stream = gulp.src(SERVER_CODE)   // Compile only the es8 code
    .pipe(cache.filter())
    .pipe(babel())
    .pipe(cache.cache())
    .pipe(gulp.dest('./build'));
  return stream;
});

// handover running and watching files for changes to gulp-nodemon
gulp.task('watch', ['compile'], () => {
  const stream = nodemon({
    script: './build/develop.js',
    watch: 'server',
    tasks: ['compile']
  });
  return stream;
});

gulp.task('default', ['watch']);

/* ------------------------- TESTING TASKS ---------------------------------- */
gulp.task('test:models', () => {
  return gulp.src('./tests/server/models/*.spec.es6')
  .pipe(mocha(mochaOpts));
});

gulp.task('test:routes', () => {
  return gulp.src('./tests/server/routes/*.spec.es6')
  .pipe(mocha(mochaOpts));
});

gulp.task('test:api', () => {
  return gulp.src('./tests/server/api/*.spec.es6')
  .pipe(mocha(mochaOpts));
});

gulp.task('test', () => runSeq('test:models', 'test:routes', 'test:api'));
